# pylint: disable=missing-module-docstring, missing-function-docstring
import nox # type: ignore

nox_settings = {
    "python": "3.9",
    "venv_backend": "venv",
    "reuse_venv": True
}

@nox.session(**nox_settings)
def lint(session):
    to_lint = "src", "tests", "noxfile.py"
    session.run("pip", "install", "-e", ".", silent=True)
    session.install("pylint", "nox", "pytest")
    session.run("pylint", *to_lint)

@nox.session(**nox_settings)
def typing(session):
    session.run("pip", "install", "-e", ".", silent=True)
    session.run("pip", "install", "pandas-stubs")
    session.install("mypy")
    session.run("mypy", "src/")

@nox.session(**nox_settings)
def docs(session):
    session.run("pip", "install", "-e", ".", silent=True)
    session.install("sphinx")
    session.run("make", "-C", "docs", "html", external=True)

#@nox.session(**nox_settings)
#def tests(session):
#    session.run("pip", "install", "-e", ".", silent=True)
#    session.install("pytest", "pytest-cov", "mysqlclient")
#    session.run("pytest")
