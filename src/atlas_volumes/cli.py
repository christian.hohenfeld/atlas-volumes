"""Command line entry points."""

import sys
from pathlib import Path
from . import atlas
from . import table

def atlas_volumes_cli() -> None:
    """Command line."""
    input_atlas = Path(sys.argv[1])
    input_info = Path(sys.argv[2])
    min_vol = float(sys.argv[3])
    output = Path(sys.argv[4])

    atlas_vols = atlas.read_atlas(atlas_file=input_atlas)
    atlas_named = atlas.add_names(info_file=input_info, atlas_dict=atlas_vols)
    table.to_table(named_dict=atlas_named, min_vol=min_vol, save_at=output)
