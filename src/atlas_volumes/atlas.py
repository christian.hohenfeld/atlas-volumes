"""Read nifti atlas files and report volume sizes"""

import csv
from pathlib import Path
import nibabel as nb # type: ignore
import numpy as np

def read_atlas(atlas_file: Path) -> dict:
    """Count the voxels per atlas region.

    Args:
        atlas_file: The atlas file to read.
    Returns:
        A dictionary with atlas region numbers as keys and voxel volume
        in cubic milimetres.

    """
    atlas = nb.load(atlas_file)
    atlas_array = atlas.get_fdata()
    max_idx = np.max(atlas_array)

    voxel_volume = np.prod(atlas.header["pixdim"][1:4])

    count_dict = {}
    # max_idx is float, range needs int
    for i in range(int(max_idx) + 1):
        count_dict[i] = len(atlas_array[atlas_array == i]) * voxel_volume
    return count_dict


def add_names(info_file: Path, atlas_dict: dict) -> dict:
    """Add human readable names to the atlas information.

    Atlas information is normally encoded with integers, but there is
    usually a list of the association between integers and names of the
    associated regions.

    Args:
        info_file: Path to a tabular text file with the first column
        containing integer keys and the second column containing names
        of brain regions.
        atlas_dict: A dictionary of voxel counts with integer indices.
    Returns:
        The dictionary supplied as `atlas_dict` but with keys replayed
        by human readable names.

    """
    key_association = {}
    with info_file.open() as info:
        info_reader = csv.reader(info, delimiter=" ")
        for row in info_reader:
            # numeric index will be stored as string, so make it an int
            key_association[int(row[0])] = row[1]

    new_dict: dict[str, int] = {}
    for i in atlas_dict.keys():
        try:
            if atlas_dict[i] == 0:
                continue
            new_dict[key_association[i]] = atlas_dict[i]
        except Exception: # pylint: disable=broad-except
            continue

    return new_dict
