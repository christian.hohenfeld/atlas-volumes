"""Turn results into a table for furhter procesing."""

import os
from pathlib import Path
import pandas as pd

def to_table(named_dict: dict, min_vol: float, save_at: Path) -> None:
    """Write volumes to table.

    Take the calculated volumes from an atlas and write them to a table.
    Volumes that are considered too small are flagged.

    Args:
        named_dict: A dictionary with regions as keys and volumes as labels.
        min_vol: The minimum volume a region should have (in cubic mm).
        save_at:
    """
    if os.path.exists(save_at):
        raise FileExistsError
    vol_table = pd.DataFrame({
        "region": named_dict.keys(),
        "volume": named_dict.values()
    })
    vol_table["flag"] = vol_table["volume"] < min_vol
    vol_table["missing"] = vol_table["volume"] - min_vol
    vol_table.loc[~vol_table["flag"], "missing"] = 0
    vol_table.to_csv(path_or_buf=save_at, sep="\t")
