=============
atlas_volumes
=============

The atlas_volumes utility calculates volume sizes form brain altasses in the 
Nifti format. This is for example useful if you want to merge small regions 
that are too small for the resolution of the data you are using.

Note
====

This project has been set up using PyScaffold 4.1. For details and usage
information on PyScaffold see https://pyscaffold.org/.
